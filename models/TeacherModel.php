<?php

class TeacherModel
{
    //Database Connection
    protected $conn;
    public function __construct()
    {
        $this->conn = App::get('query');
    }

    //Show list of all current teachers
    public function showAll()
    {
        $sql = "SELECT * FROM wfflix.teachers WHERE updated_at IS NOT NULL AND deleted_at IS NOT NULL";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    //add teacher
    public function add()
    {
        $sql = "INSERT INTO wfflix.teachers (name) VALUE ('{$_POST['teachername']}')";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    //soft-delete teacher (insert value into "deleted_at")
    public function delete()
    {
        $sql = "INSERT INTO wfflix.teachers (deleted_at) VALUE (current_timestamp)";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    //change teacher name step 1
    public function update()
    {
        $sql = "SELECT * FROM wfflix.teachers WHERE id = {$_POST['id']}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    //change teacher name step 2
    public function store()
    {
        $sql = "UPDATE wfflix.teachers SET name = '{$_POST['teachername']}',
                updated_at = current_timestamp
                WHERE id = {$_POST['teacherid']}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        header("Location: teacher");
    }
}