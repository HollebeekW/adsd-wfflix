<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'App' => $baseDir . '/core/App.php',
    'ComposerAutoloaderInit76079bb9d3734fbfd0322b00703f6916' => $vendorDir . '/composer/autoload_real.php',
    'Composer\\Autoload\\ClassLoader' => $vendorDir . '/composer/ClassLoader.php',
    'Composer\\Autoload\\ComposerStaticInit76079bb9d3734fbfd0322b00703f6916' => $vendorDir . '/composer/autoload_static.php',
    'Composer\\InstalledVersions' => $vendorDir . '/composer/InstalledVersions.php',
    'Connection' => $baseDir . '/database/Connection.php',
    'CourseController' => $baseDir . '/controllers/CourseController.php',
    'CourseModel' => $baseDir . '/models/CourseModel.php',
    'PagesController' => $baseDir . '/controllers/PagesController.php',
    'Request' => $baseDir . '/core/Request.php',
    'Router' => $baseDir . '/core/Router.php',
    'TeacherController' => $baseDir . '/controllers/TeacherController.php',
    'TeacherModel' => $baseDir . '/models/TeacherModel.php',
);
