<?php

require 'vendor/autoload.php';
require 'core/helpers.php';

App::bind('config', require 'config.php');

//Database connection
App::bind('query', Connection::make(
    App::get('config')['database']));

//Routing
Router::load('routes.php')
    ->direct(Request::uri(), Request::method()); //chain