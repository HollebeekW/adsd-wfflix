<?php

class Router
{
    protected $routes = [
        'GET' => [],
        'POST' => []
    ];

    public static function load($file)
    {
        $router = new static; //$router = new Router();
        require $file;
        return $router;
    }

    //GET
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    //POST
    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    //method direct
    public function direct($uri, $requestType)
    {
        if (array_key_exists($uri, $this->routes[$requestType])) {
            return $this->callAction(
                ...explode('@', $this->routes[$requestType][$uri])
            );
        }
    }

    protected function callAction($controller, $action) {
        $controller = new $controller;
        if(!method_exists($controller, $action)) {
            throw new Exception('Method not existent in class');
        }
        return $controller->$action();
    }

}