<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Hello, Teachers!</title>

</head>
<body>
<!-- Navigation -->
<?php require 'utils/navigation.php' ?>
<!-- End navigation -->
<div class="container-fluid">
    <div class="row">
        <div class="col-8 offset-2">
            <h1>Hello, Teachers!</h1>
            <!-- show added teachers -->
            <div class="row row-cols-1 row-cols-3 g-4 mt-5 ml-2 mr-2 p-2">
                <?php foreach ($teachers as $teacher) : ?>
                    <div class="col">
                        <div class="card mt-3">
                            <div class="card-body">
                                <h5 class="card-title"><?= $teacher['name']; ?></h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat.</p>
                                <p>
                                <form action="del-teacher" method="post">
                                    <button type="submit" name="id" value="<?= $teacher['id']; ?>"
                                            class="btn btn-danger">DELETE
                                    </button>
                                </form>
                                <form action="upd-teacher" method="post">
                                    <button type="submit" name="id" value="<?= $teacher['id']; ?>"
                                            class="btn btn-primary">UPDATE
                                    </button>
                                </form>

                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <h2>Add teacher</h2>
            <!-- Form to add teacher -->
            <form action="add-teacher" method="post">
                <div class="mb-3">
                    <label for="teachername" class="form-label">Teachers name</label>
                    <input type="text" class="form-control w-25" name="teachername" id="teachername">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>