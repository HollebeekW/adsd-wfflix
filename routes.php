<?php

//GET
$router->get('', 'PagesController@home');
//$router->get('student', 'controllers/student.php');
$router->get('teacher', 'TeacherController@home');
$router->get('about', 'PagesController@about');
//$router->get('course', 'controllers/course.php');
$router->get('contact', 'PagesController@contact');

//POST teacher
$router->post('add-teacher', 'TeacherController@add');
$router->post('del-teacher', 'TeacherController@delete');
$router->post('upd-teacher', 'TeacherController@update');
$router->post('store-teacher', 'TeacherController@store');

