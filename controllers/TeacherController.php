<?php

require 'models/TeacherModel.php';

class TeacherController
{
    public function __construct()
    {
        $this->teacher = new TeacherModel();
    }

    public function home()
    {
        $teachers = $this->teacher->showAll();

        return view('teacher', compact('teachers'));
    }

    public function add()
    {
        $this->teacher->add();

        header("Location: teacher");
    }

    public function delete()
    {
        $this->teacher->delete();

        header("Location: teacher");
    }

    public function update()
    {
        $teacher = $this->teacher->update();

        return view('teacher-update', compact('teacher'));
    }

    public function store()
    {
        $this->teacher->store();

        header("Location: teacher");
    }
}